import java.util.List;

/**
 * Created by Marlena on 14.10.16.
 */
public class Person {

    String firstName;
    String lastName;
    List<Address> addresses;
    List<PhoneNumber> phoneNumbers;
    List<Role> roles;
}
